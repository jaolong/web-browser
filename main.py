"""
-------------------------------------------------
   File Name：     main
   Description :
   Author :       willis
   date：          2019/5/11
-------------------------------------------------
   Change Activity:
                   2019/5/11:

-------------------------------------------------

"""
__author__ = 'willis'

import logging.handlers
import logging
import sys
import os



class Main:
    def __init__(self):
        self.Log_Out()

    def Log_Out(self):
        '''
        日志输出
        :return:
        '''

        log_file = os.path.realpath(sys.argv[0]) + '.log'

        # 定义对应的程序模块名name，默认是root
        logs = logging.getLogger()
        logs.setLevel(logging.DEBUG)

        # 定义日志回滚
        logrota = logging.handlers.RotatingFileHandler(log_file, maxBytes=10 * 1024 * 1024, backupCount=20)

        # 设置日志等级
        logrota.setLevel(logging.DEBUG)

        # 日志输出到屏幕控制台
        console = logging.StreamHandler()
        console.setLevel(logging.DEBUG)

        # 定义日志格式
        format = ('%(asctime)s|%(name)s|%(levelname)s|'
                  '%(pathname)s|%(lineno)d|%(thread)s|%(process)s|%(message)s')

        # 实例化handler
        formatter = logging.Formatter(format)

        # 格式化handler
        logrota.setFormatter(formatter)
        console.setFormatter(formatter)

        # 添加handler
        logs.addHandler(logrota)
        logs.addHandler(console)

        logs.debug('my is debug')
        logs.info('my is info')
        logs.warning('my is warning')
        logs.error('my is error')
        logs.critical('my is critical')
        logs.info('Loging start ...')


if __name__ == '__main__':
    smoke = Main()

